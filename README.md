# ALION-challenge - Enlighten:

Alphabet Soup

You have been contracted from a newspaper tasked with the job of providing an answer key to their word search for the Sunday print. The newspaper's word search is a traditional game consisting of a grid of characters in which a selection of words have been hidden. You are provided with the list of words that have been hidden and must find the words within the grid of characters.


Requirements
Load a character grid with scrambled words embedded within it and a words list of the words to find.  The following conditions apply:

Within the grid of characters, the words may appear vertical, horizontal or diagonal.
Within the grid of characters, the words may appear forwards or backwards.
Words that have spaces in them will not include spaces when hidden in the grid of characters.
